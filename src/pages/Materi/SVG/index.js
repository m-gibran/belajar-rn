import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Undraw from '../../../assets/undraw_Chatting_re_j55r.svg';

const Svg = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Menggunakan File SVG</Text>
      <Undraw width={244} height={125} />
    </View>
  );
};

export default Svg;

const styles = StyleSheet.create({
  container: {padding: 20},
  title: {textAlign: 'center'},
});
