import React, {useEffect, useState} from 'react';
import {View, Image, Text} from 'react-native';

const FunctionalBased = () => {
  const [subscriber, setSubsriber] = useState(300);

  useEffect(() => {
    console.log('did mount');
    setTimeout(() => {
      setSubsriber(33022);
    }, 2000);
    return () => {
      console.log('did update');
    };
  }, [subscriber]);

  //   useEffect(() => {
  //     console.log('did update');
  //     setTimeout(() => {
  //       setSubsriber(10000);
  //     }, 2000);
  //   }, [subscriber]);

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#c8d6e5',
          alignItems: 'flex-end',
        }}>
        <View style={{backgroundColor: '#ee5253', width: 50, height: 50}} />
        <View style={{backgroundColor: '#feca57', flex: 1, height: 100}} />
        <View style={{backgroundColor: '#1dd1a1', flex: 2, height: 150}} />
        <View style={{backgroundColor: '#5f27cd', flex: 3, height: 200}} />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginTop: 20,
        }}>
        <Text>Hawds</Text>
        <Text>Hai</Text>
        <Text>awid</Text>
        <Text>woiad</Text>
        <Text>nkwd</Text>
      </View>

      <View style={{marginTop: 20, flexDirection: 'row'}}>
        <Image
          style={{height: 100, width: 100, borderRadius: 50}}
          source={{
            uri:
              'https://yt3.ggpht.com/a/AATXAJwTuzNgKRSLVIOcVTVGGr_xFKgo8LFSQF163hCKSQ=s88-c-k-c0x00ffffff-no-rj',
          }}
        />
        <View style={{marginLeft: 20, justifyContent: 'center'}}>
          <View>
            <Text style={{fontSize: 17, fontWeight: 'bold', marginBottom: 5}}>
              PewDiePie
            </Text>
          </View>
          <View>
            <Text>{subscriber} M subscribers</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default FunctionalBased;
