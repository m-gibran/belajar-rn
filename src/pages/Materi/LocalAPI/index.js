import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Image,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Axios from 'axios';

const Item = ({name, email, major, update, del}) => {
  return (
    <View style={styles.wrapper}>
      <TouchableOpacity onPress={update}>
        <Image
          source={{uri: `https://i.pravatar.cc/150?u=fake@${name}.com`}}
          style={styles.img}
        />
      </TouchableOpacity>
      <View style={styles.wrap}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.desc}>{email}</Text>
        <Text style={styles.desc}>{major}</Text>
      </View>
      <Text style={styles.delete} onPress={del}>
        X
      </Text>
    </View>
  );
};

const LocalAPI = () => {
  const [name, setname] = useState('');
  const [email, setemail] = useState('');
  const [major, setmajor] = useState('');

  const [UserData, SetUserData] = useState([]);
  const [btn, setbtn] = useState('SAVE');
  const [id, setid] = useState({});

  useEffect(() => {
    getdata();
  }, []);

  const getdata = () => {
    Axios.get('http://10.0.2.2:3004/posts')
      .then((result) => {
        SetUserData(result.data);
      })
      .catch((err) => console.log(err));
  };

  const submit = () => {
    const data = {
      name: name,
      email: email,
      major: major,
    };

    if (btn === 'SAVE') {
      Axios.post('http://10.0.2.2:3004/posts', data)
        .then((result) => console.log(result))
        .catch((err) => console.log(err));
      setname('');
      setemail('');
      setmajor('');
      getdata();
    } else if (btn === 'UPDATE') {
      Axios.put(`http://10.0.2.2:3004/posts/${id}`, data)
        .then((result) => console.log(result))
        .catch((err) => console.log(err));
      setname('');
      setemail('');
      setmajor('');
      getdata();
    }
  };

  const del = (id) => {
    Axios.delete(`http://10.0.2.2:3004/posts/${id.id}`)
      .then((result) => console.log(result))
      .catch((err) => console.log(err));
    getdata();
  };

  const selecteditem = (item) => {
    setid(item.id);
    setname(item.name);
    setemail(item.email);
    setmajor(item.major);
    setbtn('UPDATE');
  };

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.title}>Local API</Text>
        <Text>Masukan Anggota Anda</Text>
        <TextInput
          placeholder="Nama Lengkap"
          style={styles.form}
          value={name}
          onChangeText={(value) => setname(value)}
        />
        <TextInput
          placeholder="Email"
          style={styles.form}
          value={email}
          onChangeText={(value) => setemail(value)}
        />
        <TextInput
          placeholder="Jurusan"
          style={styles.form}
          value={major}
          onChangeText={(value) => setmajor(value)}
        />
        <Button title={btn} onPress={submit} />
        <View style={styles.line} />
        {UserData.map((data) => (
          <Item
            key={data.id}
            name={data.name}
            email={data.email}
            major={data.major}
            update={() => selecteditem(data)}
            del={() =>
              Alert.alert('Are you sure', 'delete this user?', [
                {
                  text: 'Cancel',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {text: 'OK', onPress: () => del(data)},
              ])
            }
          />
        ))}
      </ScrollView>
    </View>
  );
};

export default LocalAPI;

const styles = StyleSheet.create({
  container: {padding: 20},
  title: {textAlign: 'center', marginBottom: 5},
  form: {
    borderBottomWidth: 1,
    borderColor: 'blue',
    borderRadius: 10,
    backgroundColor: '#fffa',
    padding: 1,
    marginVertical: 10,
  },
  line: {backgroundColor: 'black', height: 3, marginVertical: 10},
  img: {height: 100, width: 100, borderRadius: 50},
  wrapper: {flexDirection: 'row', marginBottom: 10},
  wrap: {marginLeft: 10},
  name: {fontSize: 16, fontWeight: 'bold', color: 'blue', marginTop: 15},
  desc: {marginTop: 3, fontWeight: '200'},
  delete: {
    color: 'red',
    position: 'absolute',
    right: 0,
    fontWeight: 'bold',
    fontSize: 20,
  },
});
