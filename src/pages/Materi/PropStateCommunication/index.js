import React, {useState} from 'react';
import {View} from 'react-native';

import Cart from '../../../Component/Cart';
import Product from '../../../Component/Product';

const PropStateCom = (props) => {
  const [TotalProduct, setTotalProduct] = useState(2);
  return (
    <View>
      <Cart quantity={TotalProduct} />
      <Product click={() => setTotalProduct(TotalProduct + 1)} />
    </View>
  );
};

export default PropStateCom;
