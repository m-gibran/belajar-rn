import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, Button, Image} from 'react-native';

const ApiVanilla = () => {
  const [data, setdata] = useState({
    avatar: '',
    email: '',
    first_name: '',
    last_name: '',
  });

  const [UserData, setUserData] = useState({
    createdAt: '',
    id: '',
    job: '',
    name: '',
  });
  useEffect(() => {
    //   Pemanggilan API GET
    // fetch('https://reqres.in/api/users/2')
    //   .then((response) => response.json())
    //   .then((json) => console.log(json));
    // API POST
    // const postAPI = {
    //   name: 'morpheus',
    //   job: 'leader',
    // };
    // fetch('https://reqres.in/api/users', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(postAPI),
    // })
    //   .then((response) => response.json())
    //   .then((json) => {
    //     console.log(json);
    //   });
  }, []);

  const getData = () => {
    fetch('https://reqres.in/api/users/2')
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        setdata(json.data);
      });
  };

  const postData = () => {
    const dataforapi = {
      name: 'morpheus',
      job: 'leader',
    };

    fetch('https://reqres.in/api/users', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body: JSON.stringify(dataforapi),
    })
      .then((response) => response.json())
      .then((json) => {
        console.log('post response', json);
        setUserData(json);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Pemanggilan API dengan Vanilla Js</Text>
      <Button title="GET DATA" onPress={getData} />
      <Text>Response GET DATA</Text>
      {data.avatar.length > 0 && (
        <Image source={{uri: data.avatar}} style={styles.img} />
      )}
      <Text>
        {data.first_name} {data.last_name}
      </Text>
      <Text>{data.email}</Text>
      <View style={styles.line} />
      <Button title="POST DATA" onPress={postData} />
      <Text>Response POST DATA</Text>
      <Text>{UserData.job}</Text>
      <Text>{UserData.name}</Text>
    </View>
  );
};

export default ApiVanilla;

const styles = StyleSheet.create({
  container: {padding: 20},
  title: {textAlign: 'center'},
  line: {height: 2, backgroundColor: 'black', marginVertical: 20},
  img: {height: 100, width: 100, borderRadius: 50},
});
