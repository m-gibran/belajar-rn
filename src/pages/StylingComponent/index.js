import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import dellxps from '../../assets/dell-xps.jpg';

const StylingReactNativeComponent = () => {
  return (
    <View>
      <Text style={style.text}>Styling Component</Text>
      <View
        style={{
          width: 100,
          height: 100,
          backgroundColor: 'cyan',
          borderWidth: 2,
          borderColor: 'red',
          marginTop: 20,
          marginLeft: 130,
        }}
      />
      <View
        style={{
          marginLeft: 50,
          padding: 12,
          backgroundColor: '#F2F2F2',
          width: 212,
          borderRadius: 8,
        }}>
        <Image
          source={dellxps}
          style={{width: 180, height: 107, borderRadius: 8}}
        />
        <Text style={{fontSize: 14, fontWeight: 'bold', marginTop: 16}}>
          New Dell XPS 2020
        </Text>
        <Text
          style={{
            fontSize: 12,
            fontWeight: 'bold',
            color: '#f2994a',
            marginTop: 12,
          }}>
          Rp 30.000.000
        </Text>
        <Text style={{fontSize: 12, fontWeight: '300', marginTop: 12}}>
          Bandung Timur
        </Text>
        <View
          style={{
            backgroundColor: '#6fcf97',
            // paddingTop: 6,
            // paddingBottom: 6,
            paddingVertical: 6,
            marginTop: 20,
            borderRadius: 25,
          }}>
          <Text
            style={{
              fontSize: 14,
              fontWeight: '600',
              color: 'white',
              textAlign: 'center',
            }}>
            BELI
          </Text>
        </View>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'salmon',
    marginTop: 20,
    marginLeft: 110,
  },
});

export default StylingReactNativeComponent;
