import React, {useState} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import cart from '../../assets/cart.png';

// state: {
//   Product : 0,
//   TotalProduct :0
// }
const Cart = (props) => {
  return (
    <View style={styles.Wrapper}>
      <Text>Materi Position</Text>
      <View style={styles.cartWrapper}>
        <Image source={cart} style={styles.img} />
        <Text style={styles.notif}>{props.quantity}</Text>
      </View>
      <Text>Keranjang Anda</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  img: {
    width: 50,
    height: 50,
  },

  cartWrapper: {
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 93 / 2,
    width: 93,
    height: 93,
    justifyContent: 'center',
    alignItems: 'center',
  },

  Wrapper: {
    alignItems: 'center',
    position: 'relative',
    padding: 20,
  },

  notif: {
    fontSize: 12,
    color: 'white',
    backgroundColor: 'salmon',
    borderWidth: 1,
    borderColor: 'salmon',
    borderRadius: 25,
    width: 24,
    height: 24,
    padding: 4,
    textAlign: 'center',
    position: 'absolute',
    top: 0,
    right: 0,
  },
});

export default Cart;
