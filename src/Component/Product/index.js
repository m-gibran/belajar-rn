import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import dellxps from '../../assets/dell-xps.jpg';

const Product = (props) => {
  return (
    <View>
      <View
        style={{
          marginLeft: 50,
          padding: 12,
          backgroundColor: '#F2F2F2',
          width: 212,
          borderRadius: 8,
        }}>
        <Image
          source={dellxps}
          style={{width: 180, height: 107, borderRadius: 8}}
        />
        <Text style={{fontSize: 14, fontWeight: 'bold', marginTop: 16}}>
          New Dell XPS 2020
        </Text>
        <Text
          style={{
            fontSize: 12,
            fontWeight: 'bold',
            color: '#f2994a',
            marginTop: 12,
          }}>
          Rp 30.000.000
        </Text>
        <Text style={styles.place}>Bandung Timur</Text>
        <TouchableOpacity onPress={props.click}>
          <View style={styles.wrap}>
            <Text style={styles.buy}>BELI</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'salmon',
    marginTop: 20,
    marginLeft: 110,
  },
  place: {fontSize: 12, fontWeight: '300', marginTop: 12},
  buy: {
    fontSize: 14,
    fontWeight: '600',
    color: 'white',
    textAlign: 'center',
  },
  wrap: {
    backgroundColor: '#6fcf97',
    // paddingTop: 6,
    // paddingBottom: 6,
    paddingVertical: 6,
    marginTop: 20,
    borderRadius: 25,
  },
});

export default Product;
