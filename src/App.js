import React, {useState, useEffect} from 'react';
import {View, ScrollView} from 'react-native';

import SampleComponent from './pages/SampleComponent';
import StylingReactNativeComponent from './pages/StylingComponent';
import FlexBox from './pages/Materi/FlexBox';
import Position from './pages/Materi/Position';
import ClassBased from './pages/Materi/LifeCycle/ClassBased';
import FunctionalBased from './pages/Materi/LifeCycle/FunctionalBased';

import Cart from '../src/Component/Cart';
import Product from '../src/Component/Product';
import PropStateCom from './pages/Materi/PropStateCommunication';
import Svg from './pages/Materi/SVG';
import ApiVanilla from './pages/Materi/CallAPIVanillaJs';
import CallAPIAxios from './pages/Materi/CallAPIAxios';
import LocalAPI from './pages/Materi/LocalAPI';

const App = () => {
  const [isShow, SetIsShow] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      SetIsShow(false);
    }, 5000);
  }, []);

  return (
    <View>
      <ScrollView>
        {/* <SampleComponent />
        <StylingReactNativeComponent />
        <FlexBox />
        <Position /> */}
        {/* {isShow && <ClassBased />} */}
        {/* {isShow && <FunctionalBased />} */}

        {/* <PropStateCom />
        <Svg />
        <ApiVanilla /> */}
        {/* <CallAPIAxios /> */}
        <LocalAPI />
      </ScrollView>
    </View>
  );
};
export default App;
